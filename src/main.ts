import {Bibliotheque} from "./app/Bibliotheque";
import {Livre} from "./models/Livre";
import {BandeDessine} from "./models/BandeDessine";
import {Adherent} from "./models/Adherent";

let manteLaJolie: Bibliotheque = new Bibliotheque();

/*id, titre, dateParution, nombreExemplaire, auteur, scenariste*/
/*id, titre, dateParution, nombreExemplaire, auteur, isbn*/

let schtroumpfs = new BandeDessine("La soupe aux Schtroumpfs", new Date("2004-06-02"), 2, "Peyo", "Peyo");
let picsou = new BandeDessine( "La grande épopée de Picsou", new Date("1989-12-12"), 0, "Disney", "Disney Co.")
const ouvrages = [
    new Livre("L'origine des espèces", new Date("1858-11-24"), 15, "Charles Darwin", "2-7654-1005-4"),
    new Livre("Le meilleur des mondes", new Date("1932-01-01"),5, "Aldous Huxley", "2-7654-1005-4"),
    picsou,
    schtroumpfs
];

console.log("Ajout des ouvrages : ");
for (const ouvrage of ouvrages) {
    console.log(ouvrage.getTitre(), " : " ,manteLaJolie.ajouterOuvrage(ouvrage));
}

console.log("Recherche par titre : ");
console.log(manteLaJolie.rechercherOuvragesParTitre("L'origine des espèces"));
console.log("Recherche par titre : titre faux");
console.log(manteLaJolie.rechercherOuvragesParTitre("L'origine des fourchettes"));

let bob = new Adherent("Bob", "Leponge");
console.log("Ajout de l'adherent : ")
console.log(manteLaJolie.ajouterAdherent(bob));

console.log("Recherche de l'adherent :")
console.log(manteLaJolie.rechercheAdherent(0));
console.log("Recherche de l'adherent : id 1")
console.log(manteLaJolie.rechercheAdherent(1));

console.log("Bob emprunte les schtroumpfs : ")
console.log(manteLaJolie.emprunt(bob, schtroumpfs));
console.log("Bob emprunte picsou : ")
console.log(manteLaJolie.emprunt(bob, picsou));
console.log("Stock de picsou : ")
console.log(picsou.nombreExemplairesDisponibles());

console.log("Emprunts : ")
manteLaJolie.afficherEmprunts();
console.log("Emprunts de bob : ")
manteLaJolie.afficherEmpruntsAdherent(bob);
console.log("Emprunts de l'oeuvre id = 0 : ")
manteLaJolie.afficherEmprunteursOuvrage(0);
console.log("// Rien et c'est normal")
console.log("Emprunts de l'oeuvre id = 3 : ")
manteLaJolie.afficherEmprunteursOuvrage(3);

console.log("Afficher touttes les oeuvres :");
console.log(manteLaJolie.rechercherOuvragesParTitre(""));
console.log("Effacer oeuvre id2 :")
console.log(manteLaJolie.effacerOeuvre(2));
console.log("Afficher touttes les oeuvres, sans l'éffacé et réindexé :")
console.log(manteLaJolie.rechercherOuvragesParTitre(""));

let toto = new Adherent("Toto", "Leponge");
let tata = new Adherent("Tata", "Leponge");
console.log("Ajouts de toto et tata : ");
console.log(manteLaJolie.ajouterAdherent(toto));
console.log(manteLaJolie.ajouterAdherent(tata));

console.log("Liste des adherents : ");
manteLaJolie.listAdherents();

console.log("Suppréssion de bob et tata : ");
console.log(manteLaJolie.effacerAdherent(bob));
console.log(manteLaJolie.effacerAdherent(toto));

console.log("Liste des adherents : ");
manteLaJolie.listAdherents();
