import {Adherent} from "../models/Adherent";

export class Bibliotheque {
    private adherents: Adherent[] = [];
    private ouvrages = [];

    constructor() {}

    public ajouterAdherent(adherent: Adherent):boolean{
        try {
            adherent.setId(this.adherents.length);
            this.adherents.push(adherent);
            return true;
        } catch (e) {
            return false;
        }
    }

    public rechercheAdherent(id: number): boolean{
        for (const adherent of this.adherents) {
            if(adherent.getId() == id){
                return true;
            }
        }
        return false;
    }

    public ajouterOuvrage(ouvrage): boolean{
        try {
            ouvrage.setId(this.ouvrages.length);
            this.ouvrages.push(ouvrage);
            return true;
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    public rechercherOuvragesParTitre(titre: string){
        let finded = [];
        for (const ou of this.ouvrages) {
            if(ou.getTitre()==titre){
                finded.push(ou);
            } else if(titre == ""){
                finded.push(ou);
            }
        }
        return finded;
    }

    public emprunt(adherent: Adherent, ouvrage): boolean{

            if(ouvrage.emprunter(adherent)){
                return true;
            } else {
                return false;
            }
    }

    public afficherEmprunts(): void{
        for (const ou of this.ouvrages) {
            for (const emprunt of ou.getEmprunts()) {
                const desc = emprunt.description();
                if(desc != undefined){
                    console.log(`${ou.getTitre()} : ${desc}`);
                }
            }
        }
    }

    public afficherEmpruntsAdherent(adherent: Adherent): void{
        for (const ou of this.ouvrages) {
            for (const emprunt of ou.getEmprunts()) {
                if(Object.entries(emprunt.getAdherent()).toString() === Object.entries(adherent).toString()){
                    console.log(`${ou.getTitre()} : ${emprunt.description()}`);
                }
            }
        }
    }

    public afficherEmprunteursOuvrage(id: number){
        for (const ou of this.ouvrages) {
            if(ou.getId() == id){
                for (const emprunt of ou.getEmprunts()) {
                    console.log(`${ou.getTitre()} : ${emprunt.description()}`);
                }
            }
        }
    }

    public effacerOeuvre(id: number): boolean{
        for (const ou of this.ouvrages) {
            if(ou.getId() == id){
                this.ouvrages.splice(id,1);
                for (let i=0; i<this.ouvrages.length; i++){
                    this.ouvrages[i].setId(i);
                }
                return true;
            }
        }
        return false;
    }

    public effacerAdherent(adherent: Adherent): boolean{
        for (const ou of this.ouvrages) {
            for (const emprunt of ou.getEmprunts()) {
                if(Object.entries(emprunt.getAdherent()).toString() === Object.entries(adherent).toString()){
                    return false;
                }
            }
        }

        for (const ad of this.adherents) {
            if(ad.getId() == adherent.getId()){
                this.adherents.splice(adherent.getId(),1);
                for (let i=0; i<this.adherents.length; i++){
                    this.adherents[i].setId(i);
                }
                return true;
            }
        }
    }

    public listAdherents(): void{
        for (const ad of this.adherents) {
            console.log(ad);
        }
    }
}