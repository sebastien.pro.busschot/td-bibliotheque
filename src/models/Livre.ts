import {Emprunt} from "./Emprunt";
import {Volume} from "./Volume";

export class Livre extends Volume{
    constructor(titre: string,
              dateParution: Date,
              nombreExemplaire: number,
              auteur: string,
              private isbn: string){
        super(titre, dateParution, nombreExemplaire, auteur);
    }

    public getIsbn(): string{
        return this.isbn;
    }
}