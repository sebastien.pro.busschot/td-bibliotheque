import {Ouvrage} from "./Ouvrage";
import {Emprunt} from "./Emprunt";
import {Adherent} from "./Adherent";

export abstract class Volume extends Ouvrage{

    private emprunts: Emprunt[] = [];
    private nombreExemplaireEmprunte: number = 0;

    constructor(titre: string,
                dateParution: Date,
                private nombreExemplaire: number,
                private auteur: string) {
        super(titre, dateParution);
    }

    public emprunter(adherent: Adherent): boolean{
        if(this.nombreExemplaire > 0){
            this.nombreExemplaire--;
            this.nombreExemplaireEmprunte++;
            this.emprunts.push(new Emprunt(new Date(),adherent));

            return true;
        }
        return false;
    }

    public nombreExemplairesDisponibles(): number{
        let count = 0;
        for (const emprunt of this.emprunts) {
            if(emprunt.dejaRendu() == false){
                count++;
            }
        }
        return this.nombreExemplaire - count;
    }

    public getNombreExemplaire(): number{
        return this.nombreExemplaire;
    }

    public getNombreExemplaireEmprunte(): number{
        return this.nombreExemplaireEmprunte;
    }

    public getAuteur(): string{
        return this.auteur;
    }

    public getEmprunts(): Emprunt[]{
        return this.emprunts;
    }

    public description():string {
        const date = `${this.getDateParution().getDate()}/${this.getDateParution().getMonth()}/${this.getDateParution().getFullYear()}`;
        return `Cette oeuvre est sortie le ${date}, a été ecrit par ${this.getAuteur()} et s'intitule ${this.getTitre()}`;
    }
}
