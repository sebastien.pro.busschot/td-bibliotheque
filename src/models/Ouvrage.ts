import {DbConnect} from "../trash/DbConnect";

export abstract class Ouvrage {

    private id: number;

    constructor(private titre: string, private dateParution:Date) {}

    public getId(): number{
        return this.id;
    }

    public getTitre(): string{
        return this.titre;
    }

    public getDateParution(){
        return this.dateParution;
    }

    public setId(id: number){
        this.id = id;
    }

}