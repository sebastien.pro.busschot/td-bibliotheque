import {DbConnect} from "../trash/DbConnect";
import {ApiInterface} from "../trash/ApiInterface";

export class Adherent {

    private id: number;

    constructor(private prenom:string,
                private nom:string) {}

    public setId(id: number){
        this.id = id;
    }

    public getId():number{
        return this.id;
    }

    public getFirstname():string{
        return this.prenom;
    }

    public getLastname():string{
        return this.nom;
    }
}