import {Adherent} from "./Adherent";

export class Emprunt {
    private rendu: boolean = false;

    constructor(private dateEmprunt: Date,
                private adherent: Adherent) {}

    public getDateEmprunt(): Date{
        return this.dateEmprunt;
    }

    public getAdherent(): Adherent{
        return this.adherent;
    }

    public description():string {
        const date = `${this.dateEmprunt.getDate()}/${this.dateEmprunt.getMonth()}/${this.dateEmprunt.getFullYear()}`;
        return `${this.adherent.getFirstname()} le ${date}`;
    }

    public rendre(): void{
        this.rendu = true;
    }

    public dejaRendu(): boolean{
        return  this.rendu;
    }
}