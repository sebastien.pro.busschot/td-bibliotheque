import {Volume} from "./Volume";

export class BandeDessine extends Volume{
    constructor(titre: string,
                dateParution: Date,
                nombreExemplaire: number,
                auteur: string,
                private scenariste: string) {
        super(titre, dateParution, nombreExemplaire, auteur);
    }

    public getScenariste(): string{
        return this.scenariste;
    }
}